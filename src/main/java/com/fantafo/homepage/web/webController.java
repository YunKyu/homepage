package com.fantafo.homepage.web;

import com.fantafo.homepage.Portfolio;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Controller
@AllArgsConstructor
public class webController {
    @GetMapping("/")
    public String GetIndex(Model model){
        List<Portfolio> portfolioList = new ArrayList<>();
        //tittle , client, Date, Service, Link, img
        portfolioList.add(new Portfolio("충남대 VR 안전 교육", "충남대학교", "2018.11", "충남대학교 연구원 대상으로 VR 안전 교육 진행", "/portfolio", null,"img/portfolio/2018_8_cnu/1.jpg", null));
        portfolioList.add(new Portfolio("남양주 덕송초등학교", "덕송초등학교", "2019.12", "충남대학교 연구원 대상으로 VR 안전 교육 진행", "/portfolio", null,"img/portfolio/2020_2_nyjdeoksong/1.jpg", null));
        portfolioList.add(new Portfolio("스마트헬스케어 VR Bridge Forum", "충남대학교", "2019.12", "충남대학교 연구원 대상으로 VR 안전 교육 진행", "/portfolio",null, "img/portfolio/2019_12_SmartHA_VR_Bridge_Forum/1.jpg", null));
        portfolioList.add(new Portfolio("구리시 동인초등학교", "동인초등학교", "2018.11", "충남대학교 연구원 대상으로 VR 안전 교육 진행", "/portfolio",null, "img/portfolio/2019_11_dong-in/20191119_092315.jpg", null));
        portfolioList.add(new Portfolio("세계 걸스카우트 야영대회", "충남대학교", "2019.12", "충남대학교 연구원 대상으로 VR 안전 교육 진행", "/portfolio",null, "img/portfolio/2019_World_Girl_Scout/img_0313.jpg", null));
        portfolioList.add(new Portfolio("서울 국제관광산업박람회", "충남대학교", "2019.12", "충남대학교 연구원 대상으로 VR 안전 교육 진행", "/portfolio",null, "img/portfolio/2019_Seoul_International_Tourism_Industry_Fair/test_0026.jpg", null));

        model.addAttribute("portfolioList", portfolioList);
        return "index";
    }

    @GetMapping("/portfolio")
    public String Getportfolio(Model model){
        List<Portfolio> portfolioList = new ArrayList<>();
        //tittle , client, Date, Service, Link, img
        portfolioList.add(new Portfolio("충남대 VR 안전 교육", "충남대학교", "2018.11", "충남대학교 연구원 대상으로 VR 안전 교육 진행", "/portfolio", null,"img/portfolio/2018_8_cnu/1.jpg", null));
        portfolioList.add(new Portfolio("계성 초등학교", "계성 초등학교", "2020.2", "초등학교 FIMS 설치", "/portfolio",null, "img/portfolio/2020_2_Gyeseong/IMG_0882.jpg", null));
        portfolioList.add(new Portfolio("충남대학교병원", "충남대학교병원", "2018.12", "충남대학교 병원 FIMS 설치", "/portfolio",null, "img/portfolio/2018_12_cnuh/cnuh1.jpg", null));
        portfolioList.add(new Portfolio("공터 영어", "공터 영어", "진행중..", "공터 영어 서비스", "/portfolio","hRrdmvNf2Gc", "img/portfolio/2020_oter/oter.jpg", null));
        portfolioList.add(new Portfolio("남양주 덕송초등학교", "덕송초등학교", "2019.12", "충남대학교 연구원 대상으로 VR 안전 교육 진행", "/portfolio", null,"img/portfolio/2020_2_nyjdeoksong/1.jpg", null));
        portfolioList.add(new Portfolio("스마트헬스케어 VR Bridge Forum", "충남대학교", "2019.12", "충남대학교 연구원 대상으로 VR 안전 교육 진행", "/portfolio",null, "img/portfolio/2019_12_SmartHA_VR_Bridge_Forum/1.jpg", null));
        portfolioList.add(new Portfolio("구리시 동인초등학교", "동인초등학교", "2018.11", "충남대학교 연구원 대상으로 VR 안전 교육 진행", "/portfolio",null, "img/portfolio/2019_11_dong-in/20191119_092315.jpg", null));
        portfolioList.add(new Portfolio("세계 걸스카우트 야영대회", "충남대학교", "2019.12", "충남대학교 연구원 대상으로 VR 안전 교육 진행", "/portfolio",null, "img/portfolio/2019_World_Girl_Scout/IMG_0313.jpg", null));
        portfolioList.add(new Portfolio("서울 국제관광산업박람회", "충남대학교", "2019.12", "충남대학교 연구원 대상으로 VR 안전 교육 진행", "/portfolio",null, "img/portfolio/2019_Seoul_International_Tourism_Industry_Fair/IMG_0026.jpg", null));
        portfolioList.add(new Portfolio("초등교육박람회", "충남대학교", "2018.11", "충남대학교 연구원 대상으로 VR 안전 교육 진행", "/portfolio", null, "img/portfolio/2019_Elementary_Education_Fair/20190817_170846.jpg", null));
        portfolioList.add(new Portfolio("한국기계연구원 AR", "덕송초등학교", "2019.12", "충남대학교 연구원 대상으로 VR 안전 교육 진행", "/portfolio",null,  "img/portfolio/2019_KIMM_AR/1.jpg", null));
        portfolioList.add(new Portfolio("VRAREXPO", "충남대학교", "2019.12", "충남대학교 연구원 대상으로 VR 안전 교육 진행", "/portfolio",null,  "img/portfolio/2019_VRAREXPO/20181218_130512 copy.jpg", null));
        portfolioList.add(new Portfolio("대전엑스포 아카데미과학관 쇼룸", "동인초등학교", "2018.11", "충남대학교 연구원 대상으로 VR 안전 교육 진행", "/portfolio",null,  "img/portfolio/2019_6_Daejeon_Expo_ShowRoom/coin.jpg", null));
        portfolioList.add(new Portfolio("충남대 연구원 VR 안전교육", "충남대학교", "2019.12", "충남대학교 연구원 대상으로 VR 안전 교육 진행", "/portfolio",null,  "img/portfolio/2018_9_cnu_Researcher/IMG_8961.jpg", null));
        portfolioList.add(new Portfolio("충남대 간호학과 VR 안전교육", "충남대학교", "2019.12", "충남대학교 연구원 대상으로 VR 안전 교육 진행", "/portfolio",null,  "img/portfolio/2018_9_cnu_Nursing/IMG_9173.jpg", null));
        portfolioList.add(new Portfolio("안전관리자 보수교육", "동인초등학교", "2018.11", "충남대학교 연구원 대상으로 VR 안전 교육 진행", "/portfolio",null,  "img/portfolio/2018_Safety_Manager_Training/IMG_9568.jpg", null));
        portfolioList.add(new Portfolio("경남교육박람회", "충남대학교", "2019.12", "충남대학교 연구원 대상으로 VR 안전 교육 진행", "/portfolio",null,  "img/portfolio/2017_12_Gyeongnam_Education_Fair/IMG_7728.jpg", null));
        portfolioList.add(new Portfolio("SW박람회", "충남대학교", "2019.12", "충남대학교 연구원 대상으로 VR 안전 교육 진행", "/portfolio",null,  "img/portfolio/2017_11_swExpo/IMG_7816.jpg", null));

        model.addAttribute("portfolioList", portfolioList);

        return "portfolio";
    }

    @GetMapping("/portfolioDetail")
    public String GetportfolioDetail(@RequestParam("index") String index, Model model){
        List<Portfolio> portfolioList = new ArrayList<>();
        //tittle , client, Date, Service, Link, img
        portfolioList.add(new Portfolio("충남대 VR 안전 교육", "충남대학교", "2018.9", "충남대학교 연구원 대상으로 VR 안전 교육 진행", "/portfolio","MLCrvsmK41M","img/portfolio/2018_8_cnu/1.jpg",
                Arrays.asList("img/portfolio/2018_8_cnu/1.jpg","img/portfolio/2018_8_cnu/2.jpg", "img/portfolio/2018_8_cnu/3.jpg",
                        "img/portfolio/2018_8_cnu/4.jpg", "img/portfolio/2018_8_cnu/5.jpg")));
        portfolioList.add(new Portfolio("계성 초등학교 FIMS 납품", "계성 초등학교", "2020.2", "초등학교 FIMS 설치", "/portfolio",null, "img/portfolio/2020_2_Gyeseong/IMG_0882.jpg",
                Arrays.asList("img/portfolio/2020_2_Gyeseong/IMG_0875.jpg","img/portfolio/2020_2_Gyeseong/IMG_0876.jpg", "img/portfolio/2020_2_Gyeseong/IMG_0879.jpg",
                        "img/portfolio/2020_2_Gyeseong/IMG_0880.jpg", "img/portfolio/2020_2_Gyeseong/IMG_0882.jpg")));
        portfolioList.add(new Portfolio("충남대 VR 수술환자 체험", "충남대학교병원", "2018.12", "초등학교 FIMS 설치", "/portfolio","thq3GikfPL4", "img/portfolio/2018_12_cnuh/cnuh1.jpg",
                Arrays.asList("img/portfolio/2018_12_cnuh/cnuh1.jpg","img/portfolio/2018_12_cnuh/cnuh2.jpg", "img/portfolio/2018_12_cnuh/cnuh3.jpg",
                        "img/portfolio/2018_12_cnuh/cnuh4.jpg","img/portfolio/2018_12_cnuh/cnuh5.jpg", "img/portfolio/2018_12_cnuh/cnuh6.jpg")));
        portfolioList.add(new Portfolio("공터 영어", "공터 영어", "진행중..", "공터 영어 서비스", "/portfolio","hRrdmvNf2Gc", "img/portfolio/2020_oter/oter.jpg",
                null));
        portfolioList.add(new Portfolio("남양주 덕송초등학교", "덕송초등학교", "2019.12", "초등학생 대상으로 VR 교육 진행", "/portfolio","tpJL8EvYP64", "img/portfolio/2020_2_nyjdeoksong/1.jpg",
                Arrays.asList("img/portfolio/2020_2_nyjdeoksong/1.jpg", "img/portfolio/2020_2_nyjdeoksong/2.jpg", "img/portfolio/2020_2_nyjdeoksong/3.jpg")));
        portfolioList.add(new Portfolio("스마트헬스케어 VR Bridge Forum", "충남대학교병원", "2019.12", "FIMS 기술 발표", "/portfolio",null, "img/portfolio/2019_12_SmartHA_VR_Bridge_Forum/1.jpg",
                Arrays.asList("img/portfolio/2019_12_SmartHA_VR_Bridge_Forum/1.jpg", "img/portfolio/2019_12_SmartHA_VR_Bridge_Forum/2.jpg")));
        portfolioList.add(new Portfolio("구리시 동인초등학교", "동인초등학교", "2019.11", "초등학생 대상으로 VR 교육 진행", "/portfolio","VscimDy79hc", "img/portfolio/2019_11_dong-in/20191119_092315.jpg",
                Arrays.asList("img/portfolio/2019_11_dong-in/20191119_092315.jpg", "img/portfolio/2019_11_dong-in/20191119_092330.jpg",
                        "img/portfolio/2019_11_dong-in/20191119_093916.jpg")));
        portfolioList.add(new Portfolio("세계 걸스카우트 야영대회", "한국걸스카우트연맹", "2019.8", "걸스카우트 대상으로 VR 교육 진행", "/portfolio","hlVxgh8uiH0", "img/portfolio/2019_World_Girl_Scout/IMG_0313.jpg",
                Arrays.asList("img/portfolio/2019_World_Girl_Scout/20190806_100106.jpg", "img/portfolio/2019_World_Girl_Scout/20190807_145917_HDR.jpg",
                        "img/portfolio/2019_World_Girl_Scout/IMG_0212.jpg", "img/portfolio/2019_World_Girl_Scout/IMG_0264.jpg",
                        "img/portfolio/2019_World_Girl_Scout/IMG_0313.jpg", "img/portfolio/2019_World_Girl_Scout/IMG_0382.jpg")));
        portfolioList.add(new Portfolio("서울 국제관광산업박람회", "대전광광협회", "2019.6", "VR로 대전을 통해 홍보", "/portfolio",null, "img/portfolio/2019_Seoul_International_Tourism_Industry_Fair/IMG_0026.jpg",
                Arrays.asList("img/portfolio/2019_Seoul_International_Tourism_Industry_Fair/IMG_0026.jpg", "img/portfolio/2019_Seoul_International_Tourism_Industry_Fair/IMG_0027.jpg",
                        "img/portfolio/2019_Seoul_International_Tourism_Industry_Fair/IMG_0028.jpg")));
        portfolioList.add(new Portfolio("초등교육박람회", "UaconVR", "2019.8", "어린이, 교사 대상으로 유아 VR 교육 진행", "/portfolio", null, "img/portfolio/2019_Elementary_Education_Fair/20190817_170846.jpg",
                Arrays.asList("img/portfolio/2019_Elementary_Education_Fair/20190815_143243.jpg", "img/portfolio/2019_Elementary_Education_Fair/20190817_170846.jpg",
                        "img/portfolio/2019_Elementary_Education_Fair/IMG_0415.jpg", "img/portfolio/2019_Elementary_Education_Fair/IMG_0427.jpg",
                        "img/portfolio/2019_Elementary_Education_Fair/IMG_0431.jpg")));
        portfolioList.add(new Portfolio("한국기계연구원 AR", "한국기계연구원", "2019.8", "기계 연구원의 로봇을 AR로 증강", "/portfolio","HN7qVzeEfFw",  "img/portfolio/2019_KIMM_AR/1.jpg",
                null));
        portfolioList.add(new Portfolio("VRAREXPO", "", "2018.12", "VRAREXPO 홍보 행사", "/portfolio",null,  "img/portfolio/2019_VRAREXPO/20181218_130512 copy.jpg",
                Arrays.asList("img/portfolio/2019_VRAREXPO/20181218_130512 copy.jpg", "img/portfolio/2019_VRAREXPO/20181218_130605.jpg",
                        "img/portfolio/2019_VRAREXPO/20181218_130703.jpg", "img/portfolio/2019_VRAREXPO/20181219_114415.jpg")));
        portfolioList.add(new Portfolio("대전엑스포 아카데미과학관 쇼룸", "", "2019.6", "대전 엑스포 VR 체험 교육 진행", "/portfolio",null,  "img/portfolio/2019_6_Daejeon_Expo_ShowRoom/coin.jpg",
                Arrays.asList("img/portfolio/2019_6_Daejeon_Expo_ShowRoom/20190613_103332_HDR.jpg", "img/portfolio/2019_6_Daejeon_Expo_ShowRoom/20190613_140934_HDR.jpg",
                        "img/portfolio/2019_6_Daejeon_Expo_ShowRoom/coin1.jpg", "img/portfolio/2019_6_Daejeon_Expo_ShowRoom/coin4.jpg",
                        "img/portfolio/2019_6_Daejeon_Expo_ShowRoom/cube_2.jpg", "img/portfolio/2019_6_Daejeon_Expo_ShowRoom/cube_5.jpg",
                        "img/portfolio/2019_6_Daejeon_Expo_ShowRoom/cube_6.jpg")));
        portfolioList.add(new Portfolio("충남대 연구원 VR 안전교육", "충남대학교", "2018.9", "충남대학교 연구원 대상으로 VR 안전 교육 진행", "/portfolio",null,  "img/portfolio/2018_9_cnu_Researcher/IMG_8961.jpg",
                Arrays.asList("img/portfolio/2018_9_cnu_Researcher/IMG_8961.jpg", "img/portfolio/2018_9_cnu_Researcher/IMG_8984.jpg")));
        portfolioList.add(new Portfolio("충남대 간호학과 VR 안전교육", "충남대학교", "2018.9", "충남대학교 간호학과 대상으로 VR 안전 교육 진행", "/portfolio",null,  "img/portfolio/2018_9_cnu_Nursing/IMG_9173.jpg",
                Arrays.asList("img/portfolio/2018_9_cnu_Nursing/IMG_9168.jpg", "img/portfolio/2018_9_cnu_Nursing/IMG_9170.jpg",
                        "img/portfolio/2018_9_cnu_Nursing/IMG_9173.jpg")));
        portfolioList.add(new Portfolio("안전관리자 보수교육", "충남대학교", "2018.11", "충남대학교 안전관리자 대상으로 VR 안전 교육 진행", "/portfolio",null,  "img/portfolio/2018_Safety_Manager_Training/IMG_9568.jpg",
                Arrays.asList("img/portfolio/2018_Safety_Manager_Training/IMG_9568.jpg", "img/portfolio/2018_Safety_Manager_Training/IMG_9572.jpg")));
        portfolioList.add(new Portfolio("경남교육박람회", "", "2017.12", "VR 안전 교육 진행", "/portfolio","NpavJzLpkIA",  "img/portfolio/2017_12_Gyeongnam_Education_Fair/IMG_7728.jpg",
                Arrays.asList("img/portfolio/2017_12_Gyeongnam_Education_Fair/IMG_7728.jpg", "img/portfolio/2017_12_Gyeongnam_Education_Fair/IMG_7735.jpg",
                        "img/portfolio/2017_12_Gyeongnam_Education_Fair/IMG_7759.jpg")));
        portfolioList.add(new Portfolio("SW박람회", "", "2017.11", "", "/portfolio","s62xuy4Jqro",  "img/portfolio/2017_11_swExpo/IMG_7816.jpg",
                Arrays.asList("img/portfolio/2017_11_swExpo/IMG_7759.jpg", "img/portfolio/2017_11_swExpo/IMG_7764.jpg",
                        "img/portfolio/2017_11_swExpo/IMG_7816.jpg")));

        int number = Integer.parseInt(index);

        model.addAttribute("portfolio", portfolioList.get(number));



        return "portfolio-project";
    }

}
