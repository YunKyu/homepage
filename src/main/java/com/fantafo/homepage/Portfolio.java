package com.fantafo.homepage;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class Portfolio {
    private String tittle;
    private String client;
    private String Date;
    private String Service;
    private String Link;
    private String video;
    private String img;
    private List<String> addImgs;
}
