#!/bin/bash

REPOSITORY=/home/ubuntu/app

cd $REPOSITORY/homepage/

echo "> Git Pull"

git pull

echo "> Project Build Start"

./gradlew build

echo "> Build file copy"

cp ./build/libs/*.jar $REPOSITORY/

echo "> Determine which application pid is currently running"

CURRENT_PID=$(pgrep -f homepage)

echo "$CURRENT_PID"

if [ -z $CURRENT_PID ]; then
    echo "> No applications are currently running and will not shut down"
else
    echo "> kill -2 $CURRENT_PID"
    kill -2 $CURRENT_PID
    sleep 5
fi

echo "> Deploy new applications"

JAR_NAME=$(ls $REPOSITORY/ |grep 'homepage' | tail -n 1)

echo "> JAR Name: $JAR_NAME"

nohup java -jar $REPOSITORY/$JAR_NAME &